import java.util.Vector;

public class Ghost extends Agent {

    private static int nb_instance = 0;

    private String name;

    /**
     * This the ghost constructor.
     * @param game the game where the ghost will be added to.
     */
    public Ghost( Game game ) {
        super(game);
        this.name = "G" + String.valueOf(++nb_instance);
    }

    /**
     * This method moves the ghost in the map/grid according to the rules defined under.
     */
    public void move() {
        Direction d;

        int dist = getDistance( this.x, this.y );
        double p = Generator.gen();
        if ( 
            this.game.ghost_range == 0 ||
            dist > this.game.ghost_range ||
            p < this.game.end_purchase_ratio 
        ) {
            d = this.moves[ Generator.genInt(this.moves.length) ];
           
        } else {
            int n = this.game.n;
            int m = this.game.m;
            int distance_up = getDistance( ( n + this.x - 1 ) % n, this.y );
            int distance_down = getDistance( ( ( this.x + 1 ) % n ), this.y );
            int distance_left = getDistance( this.x, ( n + this.y - 1 ) % n );
            int distance_right = getDistance( this.x, ( this.y + 1 ) % n );
            int min_dist = dist;

            if ( min_dist > distance_up ) min_dist = distance_up;
            if ( min_dist > distance_down ) min_dist = distance_down;
            if ( min_dist > distance_left ) min_dist = distance_left;
            if ( min_dist > distance_right ) min_dist = distance_right;

            Vector<Direction> directions = new Vector<>();

            if ( min_dist == distance_up ) directions.add( Direction.UP );
            if ( min_dist == distance_down ) directions.add( Direction.DOWN );
            if ( min_dist == distance_left ) directions.add( Direction.LEFT);
            if ( min_dist == distance_right ) directions.add( Direction.RIGHT );
            Console.print(this + " : " + min_dist);
            d = directions.get( Generator.genInt( directions.size() ) );
        }

        this.moveTo(d);
    }
    /**
     * return distance if smaller than ghost detection range, otherwhise  return infinity 
     */
    public int getDistance( int gx, int gy ) {
        int n = this.game.n;
        int m = this.game.m;
        PacMan pm = this.game.pac_man;
        int dx = Math.abs( gx - pm.x );
        int dy = Math.abs( gy - pm.y );
        int dist = Math.min( dx , n - dx ) + Math.min( dy, m - dy );
        return dist;
    }

    public String toString() {
        return name;
    }
    
}