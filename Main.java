import java.util.Random;
/**
 * @author IMAD HAMDACHE
 */
public class Main {

    public static void main ( String[] args ) {
        Generator.rnd = new MTRandom( (long) 146 );
        Console.enable = true;
        Game m = new Game( 10, 10, 6 , 3, 3, 0.1 );
        boolean win=false;
        int sum_win=0;
        int sum_loss=0;
        int iterations_win=0;
        int iterations_loss=0;
        int resources_loss=0;
        win = m.simulate() ;
        /*for (int i = 0; i<100; i++){
            win = m.simulate() ;
            if (!win){
                sum_win++;
                iterations_win += m.iteration;
            }
            else{ 
                sum_loss++;
                iterations_loss += m.iteration;
                resources_loss += m.getResources();
                
            }
            m.remake();
        }
        System.out.println(" wins = " + sum_win + "\n losses = " + sum_loss);
       
        System.out.println(" it_losses = " + (double)(iterations_loss/sum_loss));
        System.out.println(" resources_left = " + (double)(resources_loss/sum_loss));
        System.out.println(" it_wins = " + (double)(iterations_win/sum_win));
*/
        System.out.println(  win ? "Game over !!" : "Victory !!");
    }
}