import java.util.List;
import java.util.Vector;

/**
 * The class PacMan represents an agent with a specific behavior described by its methods down under.
 */
public class PacMan extends Agent {

    /**
     *
     */
    private class Evaluation {
        double risk = 0;
        int loot = 0;
        Direction direction = null;

        public String toString() {
            return "Eval-" + direction;
        }

        public String toString2() {

            return "{ risk : " 
             + ( this.risk == Double.POSITIVE_INFINITY ? "inf" : this.risk )
             + " \t loot : " 
             + this.loot + " }";
        }
    }

    /**
     * This the PacMan constructor uses directly Agent's constructor.
     * @param game the game where PacMan will be playing in.
     */
    public PacMan(Game game) {
        super(game);
    }

    /**
     * This method moves PacMan in the map/grid according to the rules defined under.
     */
    public void move() {
        double min_risk = Double.POSITIVE_INFINITY;
        double max_loot = Double.NEGATIVE_INFINITY;

        Evaluation up_eval = verticalEvaluation(true);
        Evaluation down_eval = verticalEvaluation(false);
        Evaluation left_eval = horizontalEvaluation(true);
        Evaluation right_eval = horizontalEvaluation(false);

       
        Console.print("UP    : " + up_eval.toString2());
        Console.print("DOWN  : " + down_eval.toString2());
        Console.print("LEFT  : " + left_eval.toString2());
        Console.print("RIGHT : " + right_eval.toString2());

        if ( min_risk > up_eval.risk )    min_risk = up_eval.risk;
        if ( min_risk > down_eval.risk )  min_risk = down_eval.risk;
        if ( min_risk > left_eval.risk )  min_risk = left_eval.risk;
        if ( min_risk > right_eval.risk ) min_risk = right_eval.risk;

        if ( min_risk == Double.POSITIVE_INFINITY ) return;
        
        Vector<Evaluation> evaluations  = new Vector<>();
        
        if ( min_risk == up_eval.risk )    evaluations.add(up_eval);
        if ( min_risk == down_eval.risk )  evaluations.add(down_eval);
        if ( min_risk == left_eval.risk )  evaluations.add(left_eval);
        if ( min_risk == right_eval.risk ) evaluations.add(right_eval);

        for( Evaluation eval : evaluations) {
            if ( eval.loot >  max_loot ) {
                max_loot = eval.loot;
            } 
        }

        int i = 0;
        while ( i < evaluations.size() ) {
            Evaluation eval = evaluations.get(i);

            if ( eval.loot < max_loot ) {
                evaluations.remove(eval);
            } else {
                ++i;
            }
        }

        Console.print( "Possible directions : " + evaluations );
        Direction d = evaluations.get( Generator.genInt( evaluations.size() ) ).direction;
        Console.print("Chosen direction : " + d);

        this.moveTo(d);
    }

    public String toString() {
        return "PM";
    }

    /**
     * This method evaluates a vertical move (up if it gets true or down otherwise) and returns the results as an
     * Evaluation object.
     * @param isUP true to evaluation a move up false for a move down.
     * @return Object Evaluation containing the risk and loot of the move.
     */
    public Evaluation verticalEvaluation( boolean isUP ) {
        List[][] positions = this.game.positions;
        
        int x, y, yp, ym;
        int ghosts_nb, ghosts_nb_m, ghosts_nb_p;
        
        Evaluation eval = new Evaluation();
        
        if ( isUP ) {
            eval.direction = Direction.UP;
        } else {
            eval.direction = Direction.DOWN;
        }

        for (int i = 1; i <= this.game.evaluation_range; i++) {
            for (int j = 0; j <= this.game.evaluation_range - i; j++) {

                if ( isUP ) {
                    x = ((this.x - i) % this.game.n + this.game.n) % this.game.n;
                } else {                    
                    x = (this.x + i) % this.game.n;
                }
        
                if (j == 0) {
                    y = this.y;
                    ghosts_nb = positions[x][y].size();
                    if (ghosts_nb > 0) {
                        if (i == 1) {
                            eval.risk = Double.POSITIVE_INFINITY;
                            eval.loot = -1;
                            return eval;
                        } else {
                            eval.risk += ((double) ghosts_nb) / ((double) (i - 1));
                        }
                    }

                    // check if there is loot
                    if ( this.game.resources[x][y] != null ) {
                        ++eval.loot;
                    }
                } else {
                    ym = ((this.y - j) % this.game.m + this.game.m) % this.game.m;
                    yp = (this.y + j) % this.game.m;

                    ghosts_nb_m = positions[x][ym].size();
                    ghosts_nb_p = positions[x][yp].size();
                 
                    eval.risk += ((double) (ghosts_nb_m + ghosts_nb_p)) / ((double) (i + j - 1));
                    
                    // check if there is loot
                    if ( this.game.resources[x][ym] != null ) {
                        ++eval.loot;
                    }
                    if ( this.game.resources[x][yp] != null ) {
                        ++eval.loot;
                    }
                }

            }
        }
        return eval;
    }

    /**
     * This method evaluates a horizontal move (left if it gets true or right otherwise) and returns the results as an
     * Evaluation object.
     * @param isUP true to evaluation a move to the left false for a move to the right.
     * @return Object Evaluation containing the risk and loot of the move.
     */
    public Evaluation horizontalEvaluation( boolean isLeft) {
        List[][] positions = this.game.positions;

        int x, y, xp, xm;
        int ghosts_nb, ghosts_nb_m, ghosts_nb_p;
        
        Evaluation eval = new Evaluation();
        
        if ( isLeft ) {
            eval.direction = Direction.LEFT;
        } else {
            eval.direction = Direction.RIGHT;
        }

        for (int i = 1; i <= this.game.evaluation_range; i++) {
            for (int j = 0; j <= this.game.evaluation_range - i; j++) {
                if ( isLeft ) {
                    y = ((this.y - i) % this.game.m + this.game.m) % this.game.m;
                } else {
                    y = (this.y + i) % this.game.m;
                }
                
                // check one cell
                if (j == 0) {
                    x = this.x;
                    ghosts_nb = positions[x][y].size();
                    if (ghosts_nb > 0) {
                        if (i == 1) {
                            eval.risk = Double.POSITIVE_INFINITY;
                            eval.loot = -1;
                            return eval;
                        } else {
                            eval.risk += ((double) ghosts_nb) / ((double) (i - 1));
                        }
                    }

                    // check if there is loot
                    if ( this.game.resources[x][y] != null ) {
                        ++eval.loot;
                    }                    
                } else { // check symetric cells
                    xm = ((this.x - j) % this.game.n + this.game.n) % this.game.n;
                    xp = (this.x + j) % this.game.n;

                    ghosts_nb_m = positions[xm][y].size();
                    ghosts_nb_p = positions[xp][y].size();
                 
                    eval.risk += ((double) (ghosts_nb_m + ghosts_nb_p)) / ((double) (i + j - 1));

                    // check if there is loot
                    if ( this.game.resources[xm][y] != null ) {
                        ++eval.loot;
                    }   
                    if ( this.game.resources[xp][y] != null ) {
                        ++eval.loot;
                    }   
                }
            }
        }
        return eval;
    }

}