import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
    /**
     * the evaluation range of pacMan
     */
    public int evaluation_range;
    /**
     * Detection's range of the ghosts.
     */
    public int ghost_range;
    /**
     * List of ghosts in the game.
     */
    private List ghosts;
    /**
     * Object PacMan in the game.
     */
    public PacMan pac_man;
    /**
     * Map/grip of positions available;
     */
    public List[][] positions;
    /**
     * Map/grip of resources available;
     */
    public Resource[][] resources;
    /**
     * Height of the map/grid.
     */
    public int n;
    /**
     * Width of the map/grid.
     */
    public int m;
    /**
     * A counter for the resources left throughout the game.
     */
    private int resource_counter;
    /**
     * To determine if the game is over or not.
     */
    private boolean game_over = false;
    /**
     * A counter for the iterations made.
     */
    public int iteration = -1;
    /**
     * Chance for ghosts to stop chasing pacMan after detecting him.
     */
    public double end_purchase_ratio;
    /**
     * Number of ghosts in the game.
     */
    public int nb_ghosts;
    
    /**
     * This is the game's constructor
     * @param n                  height of the grid
     * @param m                  width of the grid
     * @param ghosts             number of ghosts in the game
     * @param evaluation_range   the evaluation range of pacMan
     * @param ghost_range        range of the ghosts
     * @param end_purchase_ratio chance for ghosts to stop chasing pacMan after detecting him.
     */
    public Game(int n, int m, int ghosts, int evaluation_range, int ghost_range, double end_purchase_ratio) {
        this.n = n;
        this.m = m;
        this.nb_ghosts = ghosts;
        this.resource_counter = n * m;
        this.evaluation_range = evaluation_range;
        this.ghost_range = ghost_range;
        this.end_purchase_ratio = end_purchase_ratio;

        this.positions = new List[n][m];
        this.resources = new Resource[n][m];
        this.ghosts = new ArrayList<Ghost>();

        for ( int i=0; i < this.n; i++ ) {
            for ( int j=0; j < this.m; j++ ) {
                this.positions[i][j] = new ArrayList<Agent>();
                this.resources[i][j] = new Coin();
            }
        }

        this.generateGhosts(ghosts);
        this.generatePacMan();
    }

    /**
     * This method remakes the game with the initial values.
     */
    void remake(){
        this.resource_counter = n * m;
        this.iteration = -1;

        this.positions = new List[n][m];
        this.resources = new Resource[n][m];
        this.ghosts = new ArrayList<Ghost>();

        for ( int i=0; i < this.n; i++ ) {
            for ( int j=0; j < this.m; j++ ) {
                this.positions[i][j] = new ArrayList<Agent>();
                this.resources[i][j] = new Coin();
            }
        }
        this.game_over = false;
        this.generateGhosts(this.nb_ghosts);
        this.generatePacMan();
    }

    /**
     * @return the current ressources.
     */
    public int getResources() {
        return this.resource_counter;
    }

    /**
     * This method generates a number given of ghosts.
     * @param number number of ghost to generate.
     */
    public void generateGhosts(int number) {
        int counter = number;

        while (counter > 0) {
            int i = Generator.genInt( this.n );
            int j = Generator.genInt( this.m );
            Ghost ghost = new Ghost( this );
            ghost.setPosition( i, j );
            this.ghosts.add( ghost );
            --counter;
        }
        Console.print( "Ghosts Generated : " + ghosts );
    }

    /**
     * This method creates a object PacMan for the game.
     */
    private void generatePacMan() {
        int counter = 1;

        while (counter == 1 ) {
            int i = Generator.genInt( this.n );
            int j = Generator.genInt( this.m );

            if ( this.positions[i][j].size() == 0 ) {
                this.pac_man = new PacMan( this );
                this.pac_man.setPosition( i, j );
                --counter;
            }
        }

        Console.print( "PacMan Generated !" );
    }
    /**
     * This is the simulation method; it moves the pacMan and ghosts while updating the resources
     * and incrementing the number of iterations; when this.game_over is true or there's no resources left
     * exit this method.
     * @returns true if Game Over and PacMan lost else false and he wins.
     */
    public boolean simulate() {
        while ( !this.game_over && this.resource_counter > 0 ) {
            Console.cls();
            Console.print( "Iteration = " + ++this.iteration );
            Console.print( "Resources : " + this.resource_counter);
            Console.print( this );            
            this.makeMoves();
            this.setPostions();
            this.checkMap();
            try {
                Thread.sleep(500);
            } catch ( Exception e ) {
                System.err.println( e.getMessage() );
            }
        }
        Console.cls();
        Console.print( this );            
        return this.game_over;
    }

    /**
     * This method moves PacMan and the ghosts according to the set of rules defined in each of the two classes.
     */
    public void makeMoves() {
        this.pac_man.move();
        this.ghosts.forEach( (ghost)-> ( (Ghost) ghost).move() );
    }

    /**
     * This method updates the position of PacMan and the ghosts in the game after they move successfully.
     */
    public void setPostions() {
        this.pac_man.setPosition();
        this.ghosts.forEach( (ghost)-> ( (Ghost) ghost).setPosition() );
    }

    /**
     * This method check the map/grid to update the resources after PacMan moves successfully or to end the game
     * if he gets caught by the evil ghosts.
     */
    public void checkMap() {
        int x = this.pac_man.x;
        int y = this.pac_man.y;

        if ( this.positions[x][y].size() > 1 ) {
            this.game_over = true;
            return;
        }

        if ( this.resources[x][y] != null ) {
            this.resources[x][y] = null;
            --this.resource_counter;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for ( int i=0; i < this.n; i++ ) {
            for ( int j=0; j < this.m; j++ ) {
                String s =  this.positions[i][j].size() > 0 ? 
                    this.positions[i][j].get(0).toString() : this.resources[i][j] != null ? 
                        this.resources[i][j].toString() : " ";
                sb.append( s );
                sb.append( "\t" );
            }
            sb.append( "\u000b\n" );
        }

        return sb.toString();
    }
    
} 