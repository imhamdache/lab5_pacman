import java.util.Random;

public class Generator {
    public static Random rnd = null;
    /**
     * Gets a random numer 0(inclusive) to 1(exclusive)
     * 
     * @return A number from 0 to 1
     */
    public static double gen() {
        return rnd.nextFloat();
    }

    /**
     * Gets a random number from 0(inclusive) to n(exclusive)
     * 
     * @param n
     *            The superior limit (exclusive)
     * @return A number from 0 to n-1
     */
    public static int genInt(int n)
    {
        return (int) Math.floor(rnd.nextDouble() * n);
    }
    
}