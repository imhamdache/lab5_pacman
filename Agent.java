/**
 * Abstract class for an AGENT (An abstraction of PacMan and the ghosts).
 */
public abstract class Agent {
    protected enum Direction {
        UP, RIGHT, DOWN, LEFT
    };

    protected Game game;
    protected int x;
    protected int y;

    private int next_x = 0;
    private int next_y = 0;

    public Agent( Game game ) {
        this.game = game;
    }

    public void setPosition( int x, int y ) {
        this.x = x;
        this.y = y;
        this.game.positions[x][y].add(this);
    }

    public void setPosition() {
        this.setPosition( this.next_x, this.next_y );
    }

    // public Direction[] moves = { Direction.UP, Direction.UP, Direction.RIGHT ,Direction.LEFT ,Direction.DOWN ,Direction.LEFT ,Direction.DOWN ,Direction.UP ,Direction.RIGHT};
    public Direction[] moves = { Direction.UP, Direction.LEFT, Direction.RIGHT, Direction.DOWN };

    public abstract void move();

    public void moveTo( Direction d ) {
        this.game.positions[x][y].remove( this );

        switch ( d ) {
            case UP: {
                this.next_x = this.x - 1;
                this.next_y = this.y;
                break;
            }
            case  RIGHT: {
                this.next_y = this.y + 1;
                this.next_x = this.x;
                break;
            }
            case DOWN: {
                this.next_x = this.x + 1;
                this.next_y = this.y;
                break;
            }
            case LEFT: {
                this.next_y =  this.y - 1;
                this.next_x = this.x;
                break;
            }
        }
      
        this.next_x = ( this.next_x + this.game.n ) % this.game.n;
        this.next_y = ( this.next_y + this.game.m ) % this.game.m;
    }
}