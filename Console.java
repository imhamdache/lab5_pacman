public class Console {

    public static boolean enable = true;

    public static void cls() {
        if ( Console.enable ) {
            System.out.print("\033[H\033[2J");  
            System.out.flush();  
        }
    }

    public static void print( Object  o ) {
        if ( Console.enable ) {
            System.out.println(o);
        }
    }

    public static void print( int  primitif ) {
        Console.print( String.valueOf(primitif) );
    }

    public static void print( byte  primitif ) {
        Console.print( String.valueOf(primitif) );
    }

    public static void print( boolean  primitif ) {
        Console.print( String.valueOf(primitif) );
    }

    public static void print( char  primitif ) {
        Console.print( String.valueOf(primitif) );
    }

    public static void print( double  primitif ) {
        Console.print( String.valueOf(primitif) );
    }

    public static void print( float  primitif ) {
        Console.print( String.valueOf(primitif) );
    }
}